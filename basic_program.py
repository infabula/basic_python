import sys
import utils
import more_utils as mu

print(__name__, "wordt gestart")

# optionele argumenten
def full_name(first_name, last_name="---"):
    if first_name == "Jeroen":
        sys.exit(3)
    total = first_name + ' ' + last_name
    return total

def main():
    fst_name = "Jeroen"
    lst_name = "Meijer"
    fl_name = full_name(last_name=lst_name,
                        first_name=fst_name)
    print(fl_name)
    #utils.print_welcome()
    #mu.print_welcome()
    #value = int("vier")
    #print("Klaar")


if __name__ == "__main__":  # als ik het hoofdprogramma ben
    main()
    sys.exit(0)
else:
    print("Ik word geimporteerd")