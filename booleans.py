


def shop_is_open(hour, day):
    return (hour >= 9) and (hour < 18) and not (day == 'sunday')


def is_dark(hour):
    # sanitize
    if hour < 0 or hour > 23:
        # eigenlijk kan geen resultaat worden gemaakt
        raise ValueError("hour out of range")

    # als het uur tussen 17 en 8 is, returneren we True
    if (hour >= 17) or (hour <= 8):
        return True
    else:
        return False


def time_to_close_windows(hour):
    if is_dark(hour) or is_cold():
        return True
    else:
        return False

def check_shop():
    hour = 12
    day = "monday"

    if shop_is_open(hour, day):
        print("We gaan shoppen")
    else:
        print("We gaan naar huis")


#check_shop()

def main():
    try:
        hour = int(input("Wat is het uur?"))
        if is_dark(hour):
            print("Het is donker")
        else:
            print("het is licht")

    except ValueError:
        print("Hour als Ongeldige waarde")

    except Exception as e:
            print("Er is een algemene fout.")

main()
