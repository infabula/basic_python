import os.path
import paramiko
from paramiko import SSHException
from pathlib import Path


key_path = os.path.join(Path.home(), ".ssh", "id_rsa_servertje")
#key_path = r"c:\path\to\folder\foo"
#print(key_path)
ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
#ssh.load_system_host_keys(filename="")
try:
    ssh.connect(hostname='sandbox-iosxe-latest-1.cisco.com',
                username='developer',
                password='C1sco12345',
                look_for_keys=False,
                #pkey="/home/user/.ssh/id_rsa_servertje",
                port=22)

    command = 'show ip interface brief \n'

    stdin, stdout, stderr = ssh.exec_command(command)

    errors = stderr.readlines()
    for line in errors:
        print("ERROR:", line)

    with open("interfaces.txt", 'w') as outfile:
        for line in stdout.readlines():
            outfile.write(line)

except SSHException:
    print("kan geen verbinding opbouwen")
except Exception as e:
    print("Something went wrong")
    print(e)
finally:
    ssh.close()
