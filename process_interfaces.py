import re
import os.path
import server_config


def read_file(path, file):
    file_path = os.path.join(path, file)
    with open(file_path, 'r') as infile:
        content = infile.read()
    return content


def save_to_file(devices, path, filename):
    file_path = os.path.join(path, filename)
    with open(file_path, 'w') as outfile:
        # schrijf item voor item naar de file
        for device in devices:
            outfile.write(device.strip() + '\n')


def filter_loopback_devices(text):
    loopback_devices = []
    filter = r"Loopback\d{1,3}\s+(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})"
    lines = text.split('\n')

    for line in lines:
        match = re.search(filter, line)
        if match:
            octet_1 = int(match.group(1))
            octet_2 = int(match.group(2))
            octet_3 = int(match.group(3))
            octet_4 = int(match.group(4))
            ip = [octet_1, octet_2, octet_3, octet_4]  # gestructured data
            ip_string = '.'.join(map(str, ip))
            print(ip_string)
            loopback_devices.append(line)
    return loopback_devices

def print_devices(devices):
    for device in devices:
        print(device)


def main():
    infile = server_config.infile
    outfile = server_config.outfile
    path = server_config.path

    content = read_file(path, infile)
    loopback_lines = filter_loopback_devices(content)
    #print_devices(loopback_lines)
    save_to_file(loopback_lines, path, outfile)

if __name__ == "__main__":
    main()