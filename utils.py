"""
Utils has some helper function.
"""

print(__name__, " wordt gelezen")

def print_welcome():
    """Print welcome to terminal"""
    name = input("Je naam:")
    count = 4
    print("Welkom", name, "we zijn met", count, "personen")

def add(first, second):
    """Adds two values.

    :param first:
    :param second:
    :return:
    """
    total = first + second
    return total

if __name__ == "__main__":
    print("Let's run some tests")
    total = add(5, 6)
    print("The total of add(5, 6) is", total )
