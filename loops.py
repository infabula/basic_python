import random

def is_pingable(ip):
    # TODO: ping library integreren
    return random.choice([True, False])
    # return True  # fixture

def print_inacitive_servers(start, end):
    for octet in range(start, end):
        server_ip = '127.0.0.' + str(octet)
        if not is_pingable(server_ip):
            print(server_ip)

def ask_user_for_end_octet():
    while True:
        try:
            octet = int(input("Eindoctet: "))
            if octet < 0 or octet > 255:
                print("Buiten bereik.")
            else:
                return octet
        except ValueError:
            print("Dat is geen getal.")


    return 42  # fixture



def main():
    end = ask_user_for_end_octet()
    print_inacitive_servers(start=1, end=end)

main()